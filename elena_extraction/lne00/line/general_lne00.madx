!==============================================================================================
! MADX file for LNE00 optics
!
! M.A. Fraser, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "LNE00 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../lne lne_repo";


/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6,eyn=4E-6;
 
/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";


/*****************************************************************************
 * LNE00
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "lne_repo/lne00/lne00.ele";
 call, file = "lne_repo/lne00/lne00_k.str";
 call, file = "lne_repo/lne00/lne00.seq";
 !call, file = "lne_repo/lne00/lne00.dbx"; !Presently no aperture database: to be updated
 option, echo;


/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 !call, file = "./../stitched/elena_extraction_00.inp";
 call, file = "./../stitched/elena_stitched.inp";


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * MATCH
 *******************************************************************************
 use, sequence= lne00;  
 MATCH , sequence=lne00, beta0 = INITBETA0;
        constraint, sequence=lne00,range=#e,betx=1.063904875;
        constraint, sequence=lne00,range=#e,alfx=-0.6352935269;
        constraint, sequence=lne00,range=#e,bety=4.62069;
        constraint, ,sequence=lne00, range=#e,alfy=2.25636;
        !constraint, ,sequence=lne00, range=#e,dx=1.322646/beta;
        !constraint, ,sequence=lne00, range=#e,dpx=1.192656/beta;
        vary , NAME=klne.zqmf.0006;
        vary , NAME=klne.zqmd.0007;
        vary , NAME=klne.zqmf.0013;
        vary , NAME=klne.zqmd.0014;
        LMDIF, calls = 10000 , tolerance = 1.d-6;
ENDMATCH;
 
value, klne.zqmf.0006;
value, klne.zqmd.0007;
value, klne.zqmf.0013;
value, klne.zqmd.0014;

 *******************************************************************************/
 
/*******************************************************************************
 * Twiss
 *******************************************************************************/

use, sequence= lne00;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lne00_nom.tfs";

/*************************************
* Survey
*************************************/
call, file = "./make_survey_lne00.madx"; 

/***********************************
* Cleaning up
***********************************/
system, "rm lne_repo";

stop;
