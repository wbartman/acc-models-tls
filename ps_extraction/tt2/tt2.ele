! tt2_70m_v5_newQDE163QFO215.ele
!------------------------------------------------------------------
! TT2 elements definitions, K.Hanke & M.Giovannozzi 1998
! F. Velotti (2019) rechecked and added info from F. Tecker about 
! repowering of 3 quads: https://edms.cern.ch/ui/file/1870629/1.0/PS-R-EC-0006-10-00.pdf
!------------------------------------------------------------------

! magnets are called  QFO and  QDE
! magnets of string are called *.F
! bending magnet polarities as in PS ring:
! positive angles mean bending to the right or upwards

! SECTOR 1 (PS tunnel)

POINTR   :  MARKER;
F16.QFN105   :  QUADRUPOLE, L= 0.6450, K1:= KQFO105,  TYPE=SLIM; ! Magnetic length, not yoke length
F16.BHZ117   :  RBEND,      L= 1.8500,                ANGLE= +.0150000;
F16.QDN120   :  QUADRUPOLE, L= 1.2000, K1:= KQDE120,  TYPE=FL;
F16.BVT123   :  RBEND,      L= 2.2000,                ANGLE= -.0176380, TILT=PI/2;
F16.QFN135   :  QUADRUPOLE, L= 1.2000, K1:= KQFO135,  TYPE=FL;
F16.BHZ147   :  RBEND,      L= 1.8500,                ANGLE= +.0132000;
F16.QDN150   :  QUADRUPOLE, L= 1.2000, K1:= KQDE150,  TYPE=FL;
F16.STOPPER  :  MONITOR,    L= 0.9000;
F16.QFN165   :  QUADRUPOLE, L= 1.2000, K1:= KQFO165,  TYPE=FL;
F16.BHZ167   :  RBEND,      L= 1.4000,                ANGLE= -.0085340;
F16.BVT173   :  RBEND,      L= 2.2000,                ANGLE= +.0176380, TILT=PI/2;
F16.QDN180   :  QUADRUPOLE, L= 1.2000, K1:= KQDE180,  TYPE=FL;

! New quadrupoles for Pb stripper insertion

F16.QDN163   :  QUADRUPOLE, L= 0.5000, K1:= KQDE163,  TYPE=Q130; ! new shorter 0.5m quad
F16.QDN207   :  QUADRUPOLE, L= 1.2000, K1:= KQDE207,  TYPE=FL; ! new 1.2m long quad
F16.QDN213   :  QUADRUPOLE, L= 0.8000, K1:= KQDE213,  TYPE=FS;
F16.QDN217   :  QUADRUPOLE, L= 0.8000, K1:= KQDE217,  TYPE=FS; ! old short QFO215


! STRING QUADRUPOLES

QDE220.F     :  QUADRUPOLE, L= 0.8000, K1:= KQDE220.F,TYPE=FS;
QFO225.F     :  QUADRUPOLE, L= 0.8000, K1:= KQFO225.F,TYPE=FS;
QDE240.F     :  QUADRUPOLE, L= 0.8200, K1:= KQDE240.F,TYPE=D;
QFO265.F     :  QUADRUPOLE, L= 0.8000, K1:= KQFO265.F,TYPE=FS; ! decoupled after LS2 (FT 19/12/2018)
QDE280.F     :  QUADRUPOLE, L= 0.8200, K1:= KQDE280.F,TYPE=D;  ! decoupled after LS2 (FT 19/12/2018)
QFO325.F     :  QUADRUPOLE, L= 0.8000, K1:= KQFO325.F,TYPE=FS; ! decoupled after LS2 (FT 19/12/2018)



! SECTOR 2 (TT2 tunnel)

F16.QFN205   :  QUADRUPOLE, L= 1.2000, K1:= KQFO205,  TYPE=FL;
F16.QDN210   :  QUADRUPOLE, L= 0.8200, K1:= KQDE210,  TYPE=D;
F16.QFN215   :  QUADRUPOLE, L= 1.2000, K1:= KQFO215,  TYPE=FL; ! new 1.2m long quad



! SECTOR 3 (TT2 tunnel)

F16.BTI247   :  RBEND,      L= 2.815; ! Type: D220, VB4(ISR)
F16.BTI248   :  RBEND,      L= 2.815; ! Type: D220, VB4(ISR)
ERD          :  HKICKER,    L= 0.665;! Emittance reduction dipole. The tank is 1.1916m long.


! SECTOR 4 (TT2 tunnel)

F16.BHZ327   :  RBEND,      L= 1.8500;
F16.DHZ337   :  RBEND,      L= 2.5000;
F16.DVT353   :  RBEND,      L= 2.2000,                TILT=PI/2;


! SECTOR 5 (TT2 tunnel)

F16.QFN375   :  QUADRUPOLE, L= 0.8000, K1:= KQFO375,  TYPE=FS;
LEPPU        :  MONITOR,    L= 0.7180;
TT2MTV       :  MONITOR,    L= 0.2850;
TT2TRA       :  MONITOR,    L= 0.1495;

 stopper : MARKER;

return;
