!==============================================================================================
! MADX file for FTA optics
!
! F.M. Velotti, M.A. Fraser
! based on version from AFS repository from O. Berrig and E. Benedetto (2007)
!
! Changelog: Extraction elements included from PS and taken into account to
! calculate initial conditions and stitched model as done for nTOF
! Compared with measurements taken in 2018 by M. Fraser
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "TT2/FTA optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/
system, "ln -fns ../../../../ps/scenarios/ad/4_extraction ps_ad_repo";
system, "ln -fns ../../../../ps ps_repo";
system, "ln -fns ./../../tt2 tt2_repo";
system, "ln -fns ./../line fta_repo";


/*******************************************************************************
 * Beam
 *******************************************************************************/

Beam, particle=PROTON,pc=26,exn=10.0E-6,eyn=5.0E-4;
BRHO      := BEAM->PC * 3.3356;


/*****************************************************************************
 * TT2
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "tt2_repo/tt2.ele";
 call, file = "fta_repo/tt2_fe_26_ad.str";
 call, file = "tt2_repo/tt2.seq";
 call, file = "tt2_repo/tt2.dbx";
 option, echo;



/*******************************************************************************
 * FTA line
 *******************************************************************************/
 call, file = "fta_repo/fta.ele";
 call, file = "fta_repo/fta_fe_26_k.str";
 call, file = "fta_repo/fta.seq";
 call, file = "fta_repo/fta.dbx";



/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
 tt2fta: sequence, refer=ENTRY, l = 136.3114+60.145965;
   tt2a                  , at =        0;
   fta                   , at = 136.3114;
  endsequence;



/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "tt2_ad_from_stitched_kickers.inp";



/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();

use, sequence= tt2fta;


/*******************************************************************************
 * twiss
 *******************************************************************************/


SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_tt2_fta.tfs";


/*****************************************************************************
 Calculate live initial condition for KR or any other changes in the ring
  - For now only macro to evaluate changes in KFA71
*****************************************************************************/

call, file = "load_ad_extraction.madx";

! It needs as input a KFA71 delta kick (absolute value in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! if "sign" (second argument) = 1, positive kick, negative otherwise

set, format="22.6e";
exec, calculate_extraction(0e-3, 1, twiss_ps_ad_stitched.tfs);

exec, set_ini_conditions();

ex_g = beam->ex;
ey_g = beam->ey;
dpp = 1.6e-3;

mvar1:= sqrt(table(twiss, betx) * ex_g + (table(twiss, dx) * dpp)^2) * 1e3;
mvar2 := sqrt(table(twiss, bety) * ey_g + (table(twiss, dy) * dpp)^2) * 1e3;

use, sequence= tt2fta;
select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26, mvar1, mvar2;
twiss, beta0=initbeta0, file = "twiss_tt2_fta.tfs";

value, table(twiss, TARGMA,mvar1),
       table(twiss, TARGMA,mvar2);

! Make one single tfs file for both ring and FTN transfer line using kickers
len_twiss_tl = table(twiss, tablelength);

i = 2;
option, -info;
while(i < len_twiss_tl){

    if(i == 2){
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    !new_x = x;
    !new_px = px;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_ps_tt2_fta_nom_complete.tfs";

/*************************************
* Cleaning up
*************************************/

system, "rm ps_repo";
system, "rm ps_ad_repo";
system, "rm tt2_repo";
system, "rm fta_repo";

stop;
