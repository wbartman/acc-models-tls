!==============================================================================================
! MADX file for LEIR injection line
!
! F.M. Velotti, R. Alemany
! based on version from AFS repository, JMAD models amd measurements from V. Kain and A. Hauschauer
! Pb54+ - 0.08856 GeV/c per nucleon or Ekin=4.2MeV/n and 208 nucleons
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "LEIR injection optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";


/*******************************************************************************
 * ITH.   From LINAC3 to ITE
 *******************************************************************************/
 call, file = "./ith_2016.str"; /* Optics from Verena and Alex */
 call, file = "./ith.ele";
 call, file = "./ith.seq";

/*******************************************************************************
 * ITE = The loop.   From ITH to ETL
 *******************************************************************************/
! calling files with the new injection BPMs
 call, file = "./ite_2016.str"; /* Optics from Verena and Alex IN 2016 */
 call, file = "./ite.ele";
 call, file = "./ite.seq";

/*******************************************************************************
 * ETL.   From ITE to EE
 *******************************************************************************/
! calling files with the new injection BPMs
 call, file = "./etl_inj_2016.str"; /* Optics from Verena and Alex IN 2016 */
 call, file = "./etl.ele";
 call, file = "./etl_ext.seq";

/*******************************************************************************
 * EI.    From ETL to injection into LEIR
 *******************************************************************************/
! calling files with the new injection BPMs
 call, file = "./ei_2016.str";
 call, file = "./ei.ele";
 call, file = "./ei.seq";

/*******************************************************************************
 * set initial twiss parameters
 *******************************************************************************/
 call, file = "./leir_injection.inp";

/********************************************************************************
 * store initial parameters in memory block
 ********************************************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;

/*******************************************************************************
 * Beam definition
 *******************************************************************************/
! Lead ions - http://project-i-lhc.web.cern.ch/project-i-lhc/OperationLEIR/OverviewLEIR.pdf
! 0.0042 GeV/n = Ekin = Etotal-E0 = mc^2-E0 = SQRT[E0^2+(pc)^2]
! i.e. p=0.08856 GeV/c, corresponding to a magnetic rigidity of 1.138Tm [=208*0.08856/(54*0.2998)]
! The normalized emittances are: en=26.3E-6 and since beta*gamma=0.095, then e=2.5E-6
 dpp   = 0.004; ! energy spread
! amu  = 0.9315; ! [GeV/c^2], http://en.wikipedia.org/wiki/Atomic_mass_unit
A      = 208;    ! sum of protons and neutrons
Beam, particle=lead, pc=0.08856*A, ex=2.5E-6, ey=2.5E-6, charge=54, mass=A*0.9315, BUNCHED;

/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
! ETL for injection created as reflection of ETL for extraction

use, period=etl_ext, range=#s/#e;
seqedit, sequence=etl_ext;
    flatten;
    reflect;
    flatten;
endedit;

use, sequence=etl_ext;

leir_injection : sequence, refer=ENTRY , l  = 110.54802;
    ITH.D10                             , at =    0.9560;
    ith                                 , at =    1.5121;
    ite                                 , at =   18.9641;
    etl_ext                             , at =   33.5499;
    etl.bhn20   : et.b2i                , at =   94.4290-et.b2i->L/2;
    ei                                  , at =   94.7404;
endsequence;


/*******************************************************************************
 * save a sequence
 *******************************************************************************/
 !save, sequence=leir_injection, file=leir_injection.save;

/*******************************************************************************
 * Calculate optics
 *******************************************************************************/

use, sequence=leir_injection;
seqedit, sequence = leir_injection;
    flatten;
endedit;

use, sequence=leir_injection;
set,  format="10.6f"; ! the format of numbers in the twiss output file
set,  format="-18s";

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_leir_injection_nom.tfs";

call, file = "./make_survey.madx";

stop;


