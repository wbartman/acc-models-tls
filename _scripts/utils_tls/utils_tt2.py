import numpy as np


class i2k_tt2:
    def __init__(self):

        self.qfls = ['QDE120', 'QFO135', 'QDE150', 'QFO165', 'QDE180', 'QFO205','QDE207', 'QFO215']
        self.qfs = ['QFO225S', 'QFO375']
        self.qds = ['QDE210', 'QDE220S','QDE240S']
        self.qf101 = ['QFO105']

        self.L_QFL = 1.2
        self.I_QFL = np.poly1d([0.1443575e-18, 
                                0, 
                                -0.1635244e-12, 
                                0, 
                                0.132954e-7, 
                                0, 
                                0.5123159e-1, 
                                0])
        self.L_QFS = 0.8
        self.I_QFS = np.poly1d([0.1239355e-18,
                                0,
                                -0.1239243e-12,
                                0,
                                0.9651494e-8,
                                0,
                                0.3481475e-1,
                                0])
        self.L_QD = 0.82
        self.I_QD = np.poly1d([-0.6931895e-13,
                               0,
                               0.6142503e-8,
                               0,
                               0.3555191e-1,
                               0])
        self.L_QDF101 = 0.645
        self.I_QF101_1 = np.poly1d([-0.000000506,
                                    0.021749,
                                    -0.004])
        self.I_QF101_2 = np.poly1d([-0.000000141,
                                    0.021506,
                                    0.006])
    def kQFL(self, I, p):
        return self.I_QFL(I) / (3.3356*p) / self.L_QFL

    def kQFS(self, I, p):
        return self.I_QFS(I) / (3.3356*p) / self.L_QFS
     
    def TF_QFS(self, I):
        return self.I_QFS(I)

    def kQD(self, I, p):
        return self.I_QD(I) / (3.3356*p) / self.L_QD

    def TF_QD(self, I):
        return self.I_QD(I)

    def kQF101(self, I, p):
        return (self.I_QF101_1(I) + self.I_QF101_2(I)) / 2 / (3.3356*p) / self.L_QDF101
            
    def _zero(self, I, p):
        return np.zeros_like(I)

    def _get_tf(self, ele):

        if ele in self.qfls:
            return self.kQFL
        elif ele in self.qfs:
            return self.kQFS
        elif ele in self.qds:
            return self.kQD
        elif ele in self.qf101:
            return self.kQF101
        else:
            return self._zero

    def convert(self, element, current, p):
        if hasattr(element, '__len__'):
            output = {}
            for i, ele in enumerate(element):
                print(ele)

                output[ele] = self._get_tf(ele)(current[i], p[i])
            return output
        else:
            return self._get_tf(element)(current, p)

if __name__ == '__main__':

    # LSA settings for MTE (OPERATIONAL as SFTPRO1: MTE_2018_):

    MTE = {}
    MTE['F16.BHZ117/SettingPPM#current'] = 102.1
    MTE['F16.BHZ147/SettingPPM#current'] = 92.76
    MTE['F16.BHZ167/SettingPPM#current'] = 76.8
    MTE['F16.BHZ377/SettingPPM#current'] = 0.0
    MTE['F16.BHZ377FTS/SettingPPM#current'] = 224.29
    MTE['F16.BTI247/SettingPPM#current'] = 0.0
    MTE['F16.BTI247FTA/SettingPPM#current'] =  0.0
    MTE['F16.BVT123/SettingPPM#current'] = 146.36
    MTE['F16.BVT173/SettingPPM#current'] = 147.01
    MTE['F16.DHZ327/SettingPPM#current'] = 1.0
    MTE['F16.DHZ337/SettingPPM#current'] = -2.62
    MTE['F16.DVT353/SettingPPM#current'] = -2.58
    MTE['F16.QDE120/SettingPPM#current'] = 109.5
    MTE['F16.QDE150/SettingPPM#current'] = 62.21
    MTE['F16.QDE163/SettingPPM#current'] = 0.0
    MTE['F16.QDE180/SettingPPM#current'] = 77.41
    MTE['F16.QDE207/SettingPPM#current'] = 0.0
    MTE['F16.QDE210/SettingPPM#current'] = 124.56
    MTE['F16.QDE213/SettingPPM#current'] = 0.0
    MTE['F16.QDE217/SettingPPM#current'] = 0.0
    MTE['F16.QDE220S/SettingPPM#current'] = 135.38
    MTE['F16.QFO105/SettingPPM#current'] = 322.8
    MTE['F16.QFO135/SettingPPM#current'] = 70.47
    MTE['F16.QFO165/SettingPPM#current'] = 82.65
    MTE['F16.QFO205/SettingPPM#current'] = 82.34
    MTE['F16.QFO215/SettingPPM#current'] = 98.9
    MTE['F16.QFO225S/SettingPPM#current'] = 137.75
    MTE['F16.QFO375/SettingPPM#current'] =  141.72
    MTE['F16.SNP208/SettingPPM#current'] = 0.0
    MTE['F16.SNP228/SettingPPM#current'] = 0.0

    # Filter dataset for LSA quad values:
    MTE['LSA'] = {}
    MTE['LSA']['I'] = {}
    for i in MTE.keys():
        if i[0:3] == 'F16':
            test = i.split('F16.')[1].split('/')[0]
            if test[0:2] == 'QF':
                MTE['LSA']['I'][test] = MTE[i]
            if test[0:2] == 'QD':
                MTE['LSA']['I'][test] = -MTE[i]

    print(MTE['LSA']['I'].keys())
    print(MTE['LSA']['I'].values())

    elements = list(MTE['LSA']['I'].keys())
    currents = list(MTE['LSA']['I'].values())

    tt2_k = i2k_tt2()
    tt2_k.convert(elements, currents, [14] * len(elements))

    elements = ['QDE120', 'QDE150', 'QDE163', 'QDE180', 'QDE207', 'QDE210', 'QDE213', 'QDE217', 'QDE220S', 'QFO105', 'QFO135', 'QFO165', 'QFO205', 'QFO215', 'QFO225S', 'QFO375']
    currents = [-109.5, -62.21, -0.0, -77.41, -0.0, -124.56, -0.0, -0.0, -135.38, 322.8, 70.47, 82.65, 82.34, 98.9, 137.75, 141.72]


    max_k = tt2_k.convert(elements, [400] * len(elements), [14] * len(elements))

    [print('Ele: {} => max: {}'.format(key, max_k[key])) for key in max_k.keys()]
