<h1> Transfer Lines Optics Repository </h1>

This website contains the official optics models for the CERN transfer lines.
For each transfer line listed below MAD-X input scripts, Twiss tables and
optics plots are available and can be browsed.
For each line, a model including the accelerator upstream (or downstream) is also included.
The injection lines include only the stitched model of TL + downstream machine, instead
the extraction lines also include the models for the single TLs. 

<h2> CERN Transfer Lines groups:</h2>

{% for idx, row in scenarios.iterrows() %}
- [{{ row['label'] }}]({{idx}}/index.md) - {{ row['short_desc'] }}
{% endfor %}

<h2> Data structure </h2>

The data for the different TL zones quoted above are organised in the following way:

<ul>
<li><b>Injection</b>: for each TL group quoted as "injection", only the stitched model ("stitched") of the TL with the
downstream
machine is available.
</li>

<li><b>Extraction</b>: for each TL group quoted as "extraction", two models are available: pure TL ("line") and
stitched ("stitched") model
with
 the upstream machine.
</li>

</ul>
